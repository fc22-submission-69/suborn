## Setup instructions

After cloning the repository and going into its directory, run `npm install`. To
verify that it works correctly, run `npm test` after installation is complete.
