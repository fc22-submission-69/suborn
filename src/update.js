'use strict'

const bcoin = require('bcoin')
const assert = require('bcoin/node_modules/bsert')

const Scripts = require('./scripts')
const Utils = require('./utils')

const MTX = bcoin.MTX
const Coin = bcoin.Coin
const Script = bcoin.Script

function verifyArgs(rings, amounts, localHash, remoteHash) {
  Object.values(rings).map(Utils.ensureWitness)
  Object.values(rings).map(ring => Utils.publicKeyVerify(ring.publicKey))
  Object.values(amounts).map(Utils.amountVerify)
  Utils.hashVerify(localHash)
  Utils.hashVerify(remoteHash)
}

function getLocalOutput(key, hash) {
  const redeemScript = Scripts.localScript(key, hash)
  return Utils.outputScrFromRedeemScr(redeemScript)
}

function getRemoteOutput(key, hash) {
  const redeemScript = Scripts.updateRemoteScript(key, hash)
  return Utils.outputScrFromRedeemScr(redeemScript)
}

function getUpdateTX({
  rings: {localSetupRing, remoteSetupRing, localUpdateRing, remoteUpdateRing},
  amounts: {localAmount, remoteAmount},
  localHash, remoteHash, setupTX
}) {
  const arg = arguments[0]
  verifyArgs(arg.rings, arg.amounts, arg.localHash, arg.remoteHash)

  localSetupRing.script = remoteSetupRing.script = Script.fromMultisig(2, 2, [
    localSetupRing.publicKey, remoteSetupRing.publicKey
  ])
  const prevoutScript = Utils.outputScrFromRedeemScr(localSetupRing.script)

  let updateTX = new MTX({version: 2})

  const localOutput = getLocalOutput(localUpdateRing.publicKey, localHash)
  updateTX.addOutput(localOutput, localAmount)

  const remoteOutput = getRemoteOutput(remoteUpdateRing.publicKey, remoteHash)
  updateTX.addOutput(remoteOutput, remoteAmount)

  const coin = Utils.getCoinFromTX(prevoutScript.toJSON(), setupTX, 0)
  updateTX.addCoin(coin)

  updateTX.sign([localSetupRing, remoteSetupRing])

  return updateTX
}

module.exports = getUpdateTX
