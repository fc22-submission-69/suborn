'use strict'

const bcoin = require('bcoin')
const assert = require('bcoin/node_modules/bsert')
const sha256 = require('bcoin/node_modules/bcrypto').SHA256

const Utils = require('./utils')
const Scripts = require('./scripts')
const Update = require('./update')

const MTX = bcoin.MTX
const Script = bcoin.Script
const Coin = bcoin.Coin

function getPunishTX({
  rings: {updateRing, minerRing},
  fee, preimage, hash, updateTX
}) {
  const punishTX = new MTX({version: 2})

  const redeemScript = Script.fromAddress(minerRing.getAddress())
  const output = Utils.outputScrFromRedeemScr(redeemScript)
  const amount = updateTX.outputs[1].value - fee
  punishTX.addOutput(output, amount)

  const prevoutRedeemScript = Scripts.updateRemoteScript(updateRing.publicKey, hash)
  const prevoutScript = Utils.outputScrFromRedeemScr(prevoutRedeemScript)
  const coin = Utils.getCoinFromTX(prevoutScript.toJSON(), updateTX, 0)
  punishTX.addCoin(coin)

  Utils.compile(punishTX, prevoutRedeemScript, [preimage], 0, Scripts.punishWitScr())

  return punishTX
}

module.exports = getPunishTX
