'use strict'

module.exports = {
  getSetupTX: require('./setup'),
  getUpdateTX: require('./update'),
  getRefundTX: require('./refund'),
  getPunishTX: require('./punish'),
}
