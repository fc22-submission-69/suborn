'use strict'

const bcoin = require('bcoin')
const bcrypto = require('bcoin/node_modules/bcrypto')
const sha256 = bcrypto.SHA256
const assert = require('bcoin/node_modules/bsert')

const KeyRing = bcoin.KeyRing
const Amount = bcoin.Amount
const Script = bcoin.Script
const Outpoint = bcoin.Outpoint
const MTX = bcoin.MTX
const Input = bcoin.Input
const Witness = bcoin.Witness
const Coin = bcoin.Coin

const DMC = require('../src/dmc')

const NUM_HASHES = 2 // 1 per player
// TODO: decide preimage length
const PREIMAGE_LENGTH = 8

const rings = Array.apply(null, Array(8)).map(x => KeyRing.generate())
rings.map(ring => {ring.witness = true})

const preimages = Array.apply(null, Array(NUM_HASHES))
      .map(x => bcrypto.random.randomBytes(PREIMAGE_LENGTH))
const hashes = preimages.map(sha256.digest)

const delay = 100

const setupHash = sha256.digest(Buffer.from('setup'))

const aliceAmount = Amount.fromBTC(10).toValue()
const bobAmount = Amount.fromBTC(20).toValue()
const colEpsilon = 40000
const setupFee = 2330
const refundFee = 14900
const aliceRefundFee = 7512
const bobRefundFee = 7512
const punishFee = 1500

const aliceOrigRing = rings[0]
const aliceSetupRing = rings[1]
const bobSetupRing = rings[2]
const aliceRefundRing = rings[3]
const bobRefundRing = rings[4]
const aliceUpdateRing = rings[5]
const bobUpdateRing = rings[6]
const minerRing = rings[7]

describe('End-to-end test', () => {
  const setupTX = DMC.getSetupTX({
    outpoint: new Outpoint(setupHash, 0),
    ring: aliceOrigRing,
    fundKey1: aliceSetupRing.publicKey,
    fundKey2: bobSetupRing.publicKey,
    outAmount: aliceAmount + bobAmount
  })

  describe('Setup TX', () => {
    it('should verify correctly', () => {
      assert(setupTX.verify(),
        'Setup TX does not verify correctly')
    })

    let setupTX2 = new MTX({version: 2})

    setupTX2.addCoin(Coin.fromJSON({
      version: 2,
      height: -1,
      value: aliceAmount + bobAmount,
      coinbase: false,
      script: aliceOrigRing.getProgram().toRaw().toString('hex'),
      hash: setupHash.toString('hex'),
      index: 0
    }))

    setupTX2 = DMC.getSetupTX({
      setupTX: setupTX2, fundKey1: aliceSetupRing.publicKey,
      fundKey2: bobSetupRing.publicKey, outAmount: aliceAmount + bobAmount
    })

    setupTX2.sign(aliceOrigRing)

    it('should be generatable both from MTX and from KeyRing', () => {
      assert(setupTX.hash().equals(setupTX2.hash()) &&
        setupTX.witnessHash().equals(setupTX2.witnessHash()),
        'The two setup TX generation methods do not produce same results')
    })
  })

  const refundTX = DMC.getRefundTX({
    rings: {
      localSetupRing: aliceSetupRing, remoteSetupRing: bobSetupRing,
      localRefundRing: aliceRefundRing, remoteRefundRing: bobRefundRing
    },
    amounts: {
      localAmount: aliceAmount, remoteAmount: bobAmount
    },
    preimage: preimages[1], delay, setupTX
  })

  describe('Refund TX', () => {
    it('should verify correctly', () => {
      assert(refundTX.verify(),
        'Refund TX does not verify correctly')
    })

    const setupWitnessHash = setupTX.outputs[0].script.code[1].data
    const refundWitnessScript = refundTX.inputs[0].witness.getRedeem().sha256()
    it('should spend Setup TX', () => {
      assert(
        setupWitnessHash.equals(refundWitnessScript),
        'Setup output witness hash doesn\'t correspond to refund input witness script'
      )
    })

    it('should have nLockTime in the future', () => {
      assert(
        // TODO: if a proper blockchain is set up, replace "> 0" with "== MTP + delay"
        refundTX.locktime > 0,
        'nLockTime of refundTX should be greater than the current chain height'
      )
    })
  })

  const updateTX = DMC.getUpdateTX({
    rings: {
      localSetupRing: aliceSetupRing, remoteSetupRing: bobSetupRing,
      localUpdateRing: aliceUpdateRing, remoteUpdateRing: bobUpdateRing
    },
    amounts: {
      localAmount: aliceAmount, remoteAmount: bobAmount
    },
    localHash: hashes[0], remoteHash: hashes[1], setupTX
  })

  describe('Update TX', () => {
    it('should verify correctly', () => {
      assert(updateTX.verify(),
        'Update TX does not verify correctly')
    })

    const setupWitnessHash = setupTX.outputs[0].script.code[1].data
    const updateWitnessScript = updateTX.inputs[0].witness.getRedeem().sha256()
    it('should spend Setup TX', () => {
      assert(
        setupWitnessHash.equals(updateWitnessScript),
        'Setup output witness hash doesn\'t correspond to update input witness script'
      )
    })
  })

  describe('miner Punishment TX', () => {
    const punishTX = DMC.getPunishTX({
      // TODO: think if wanting miners to know updateRing is reasonable
      rings: {updateRing: bobUpdateRing, minerRing},
      fee: punishFee, preimage: preimages[1], hash: hashes[1], updateTX
    })

    it(`TX should verify correctly`, () => {
      assert(punishTX.verify(), `punish TX does not verify correctly`)
    })

    const updateRemoteWitnessHash = updateTX.outputs[1].script.code[1].data
    const punishWitnessScript = punishTX.inputs[0].witness.getRedeem().sha256()
    it(`TX should spend remote (first) output of update TX`, () => {
      assert(
        updateRemoteWitnessHash.equals(punishWitnessScript),
        'Remote update output witness hash doesn\'t correspond to punish witness script'
      )
    })
  })
})
